// using the newest way
const arr1 = [[1, 2, [3]], 4];
let firstResult = arr1.flat(Number.MAX_SAFE_INTEGER);
console.log(firstResult);

// or recursion way
const arr2 = [0, [1, [2, [3, [4, 5], 6]]], [7, [8]], 9];
function flattenDeep(arr2) {
    return arr2.reduce((acc, val) => Array.isArray(val) ? acc.concat(flattenDeep(val)) : acc.concat(val), []);
}

let secondResult = flattenDeep(arr2);
console.log(secondResult);