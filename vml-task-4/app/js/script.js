const canvas = document.getElementById('canvas');
const ctx = canvas.getContext('2d');

const deviceWidth = window.innerWidth;
canvasWidth = Math.min(1024, deviceWidth-20);
canvasHeight = Math.min(768, deviceWidth-20);

canvas.width = canvasWidth;
canvas.height = canvasHeight;

let img = document.getElementById('default-image');
img.onload = function() {
    x = canvas.width/2 - img.width/2;
    y = canvas.height/2 - img.height/2;
    ctx.drawImage(img, x, y);
};

const uploadFile = document.getElementById('upload-file');
uploadFile.addEventListener('change', function() {
    let reader = new FileReader();
    reader.onload = function() {
        img = new Image();
        img.onload = function(){
            ctx.clearRect(0, 0, canvas.width, canvas.height);
            x = canvas.width/2 - img.width/2;
            y = canvas.height/2 - img.height/2;
            ctx.drawImage(img,x,y);
        };
        img.src = reader.result;
    };
    reader.readAsDataURL(uploadFile.files[0]);
});

const captionButton = document.getElementById('caption');
captionButton.addEventListener('click', captionImage);

function captionImage() {
    captionButton.removeEventListener('click', captionImage);
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.translate(canvas.width/2, canvas.height/2);
    ctx.translate(-canvas.width/2, -canvas.height/2);
    ctx.drawImage(img, x, y);
    addCaption();
}

function addCaption() {
    ctx.lineWidth  = 6;
    ctx.font = '66px Lato';
    ctx.strokeStyle = 'black';
    ctx.fillStyle = 'white';
    ctx.textAlign = 'center';
    ctx.lineJoin = 'round';

    let text = document.getElementById('user-text').value;
    text = text.toUpperCase();
    x = canvas.width/2;
    y = canvas.height - canvas.height/11;
    ctx.strokeText(text, x, y);
    ctx.fillText(text, x, y);
}

const refresh = document.getElementById('refresh');
refresh.addEventListener('click', function() {
    document.location.reload(true);
});

const download = document.getElementById('download');
download.addEventListener('click', function() {
    let data = canvas.toDataURL();
    download.href = data;
});