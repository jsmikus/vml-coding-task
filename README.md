# VML Coding Task

**Hi, thank you for sending coding tasks. Process of solution was very satisfying. From available tasks I chose the ones that seemed the most interesting to me. I tried to choose each one from different type.**

Please see below some additional information about the usage, my solutions and key takeaways.

---

## Table of Contents:

* [Installation](#markdown-header-installation)
* [Usage](#markdown-header-usage)
* [Tasks](#markdown-header-tasks)
    * [Task 2](#markdown-header-task-2)
    * [Task 4](#markdown-header-task-4)
    * [Task 7](#markdown-header-task-7)
* [Feedback](#markdown-header-feedback)

---

## Installation

    $ git clone https://jsmikus@bitbucket.org/jsmikus/vml-coding-task.git
    $ cd vml-task-no // in place of no put task number (2, 4 or 7)
    $ npm install

---

## Usage

### Watch & Running Development Server

    $ gulp

Then open http://localhost:3000 to see the app.

### Deploying Locally

    $ gulp build

---

## Tasks

---

### Task-2

> The top-players.png file contains a 4-box view. Please encode the component for proper display in both the mobile version (min-width: 320px) and the desktop. In the mobile version, please let the boxes be a slider that we can swipe freely. I also include the appropriate font brush.woff to do this task.

When building boxes from scratch I used Photoshop to calculate the dimensions of boxes, distance between elements and used colors. As additional font I used Montserrat from Google Fonts.

To achieve a result of swipe I used the Swiper which is also a default slider component in Ionic Framework and works very well.

PS. On the first box there I supposed that there is a typo in box title which is saying "PONTS". I did not change it to reproduce the original but in the production environment I would consult it first with the decision-maker for the content.

#### Key takeaways

1. The ability to use Photoshop directly influences the possibility of efficient coding of certain elements.
2. The proper naming of classes in CSS is the key to using SASS capabilities.
3. Sometime there is no need to reinvent the wheel but sometimes there is!

---

### Task-4

> Use the canvas to write a mechanism that will work according to the following instructions:

 > a. Step1: User added any picture from the computer
 
 > b. Step2: User adds any text to the photo (max 16 characters)
 
 > c. Step3: The user gets a complex picture with text in the form of an image that can be scanned to the computer.

When I saw that the task was based on `<canvas>`, I thought about using the Pixi.js library that I know from the work on the Tetris game which I am building in Angular. Its strength, however, consists in displaying dynamic content, so it could be an overdoing form over the content and eventually I decided on a traditional solution.

#### Key takeaways

1. I was enjoying myself by testing the solutions and creating dozens of captions, which with each subsequent test were more and more abstract.
2. I see a field to improve in the context of loading the photo's proportions.

---

### Task-7

> We would like to ask you to write a piece of code that will flatten an array of arbitrarily nested arrays of integers into one flat array of integers. For example script of the following input: 

> a. [[1,2,[3]],4]

> b. should produce the following array [ 1, 2, 3, 4 ]

Using the latest ECMA capability the `flat()` function there is a need specify the depth of the array to flat. In that case, I used maximum safe integer in JavaScript. This solution will allow to pass tests with a huge array of arrays.
 
 Second solutions is based on recursive function.

#### Key takeaways

1. I am happy about the development of ECMA. The introduction of new built-in functions such as `flat()` means that we do not necessarily have to use external libraries such as Underscore or Lodash.

## Feedback

Regardless of the recruitment result, any feedback and suggestions are very welcome. I learn every day, optimize solutions and always know that something can be done better. Feedback drives me into action, demonstrates new opportunities and motivates me. Thanks!